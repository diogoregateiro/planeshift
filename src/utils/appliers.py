import grp
import logging
import os
import shutil
import stat
import subprocess

from utils.config import UserConfig
from utils.file_marker import FileMarker

logger = logging.getLogger("Planeshift")


class Applier:
    """Generic configuration applier"""

    def apply(self, config: UserConfig) -> None:
        raise NotImplementedError()


class ShellApplier(Applier):
    """Applies the default shell configuration"""

    def apply(self, config: UserConfig) -> None:
        logger.debug(f"ShellApplier(apply)")

        if config.default_shell != config.current_shell:
            logger.debug(f"ShellApplier(apply): Changing {config.name} default shell to {config.default_shell}...")
            subprocess.run(["chsh", "-s", config.default_shell, config.name])


class SudoersApplier:
    """Applies the sudo commands configuration"""

    def apply(self, config: UserConfig) -> None:
        logger.debug(f"SudoersApplier(apply)")

        # Adjust the file path if the target is a chroot
        if config.chroot:
            sudoers_file = f"/srv/chroot/planeshift/{config.name}/etc/sudoers.d/planeshift"
        else:
            sudoers_file = "/etc/sudoers.d/planeshift"

        # Create the sudoers file if commands are specified
        sudoers = []
        # Read the planeshift sudoers file if it exists
        if os.path.exists(sudoers_file) and os.path.isfile(sudoers_file):
            logger.debug(f"SudoersApplier(apply): Reading the sudoers file ({sudoers_file})...")
            with open(sudoers_file, "r") as fd:
                sudoers = fd.readlines()

        # Write a new sudoers file
        with open(sudoers_file, "w") as fd:
            # Remove the current user from the list if it exists
            for sudoer in sudoers:
                if not sudoer.startswith(config.name):
                    fd.write(sudoer)

            # Add an entry for the user if necessary
            if config.sudo_commands:
                logger.debug(f"SudoersApplier(apply): Updating the sudoers file ({sudoers_file}) for {config.name}...")
                fd.write(f"{config.name} ALL=(root) NOPASSWD:{', '.join(config.sudo_commands)}\n")
            else:
                logger.debug(
                    f"SudoersApplier(apply): Ensuring {config.name} is not in the sudoers file ({sudoers_file})..."
                )


class SSHKeysApplier(Applier):
    """Applies the ssh keys configuration"""

    def apply(self, config: UserConfig) -> None:
        logger.debug(f"SSHKeysApplier(apply)")

        ssh_path = f"/home/{config.name}/.ssh"
        authorized_keys_path = f"{ssh_path}/authorized_keys"

        # Ensure the .ssh folder exists
        if not os.path.exists(ssh_path):
            logger.debug(f"SSHKeysApplier(apply): Creating .ssh folder ({ssh_path})...")
            os.mkdir(ssh_path)
            logger.debug(f"SSHKeysApplier(apply): Fixing permissions...")
            os.chown(ssh_path, config.uid, config.gid)
            os.chmod(ssh_path, stat.S_IRWXU)  # chmod 700

        # Open the authorized_keys file with the file marker
        fm = FileMarker(authorized_keys_path)

        # Write the ssh keys into the authorized_keys file
        logger.debug(f"SSHKeysApplier(apply): Creating the authorized_keys file ({authorized_keys_path})...")
        fm.write_section(config.ssh_keys)

        # Ensure correct owner and permissions on the authorized_keys file
        logger.debug(f"SSHKeysApplier(apply): Fixing permissions...")
        os.chown(authorized_keys_path, config.uid, config.gid)
        os.chmod(authorized_keys_path, stat.S_IRUSR | stat.S_IWUSR)  # chmod 600


class UnixGroupsApplier(Applier):
    """Applies the unix groups configuration"""

    def apply(self, config: UserConfig) -> None:
        logger.debug(f"UnixGroupsApplier(apply)")

        # Check if there are any missing groups
        curr_unix_groups = [g.gr_name for g in grp.getgrall()]
        miss_unix_groups = [g for g in config.unix_groups if g not in curr_unix_groups]
        add_unix_groups = [g for g in config.unix_groups if g not in config.user_groups]

        # Add any missing groups to the system
        for group in miss_unix_groups:
            logger.debug(f"UnixGroupsApplier(apply): Creating group {group}...")
            subprocess.run(["groupadd", group])

        # Add the user to the specified groups
        if add_unix_groups:
            logger.debug(f"UnixGroupsApplier(apply): Adding user {config.name} to group {group}...")
            subprocess.run(["usermod", "-a", "-G", ",".join(add_unix_groups), config.name])


class EnvApplier(Applier):
    """Applies the env configuration"""

    def apply(self, config: UserConfig) -> None:
        logger.debug(f"EnvApplier(apply)")

        # Open the profile or bashrc file with the file marker
        if config.chroot:
            file = f"/srv/chroot/planeshift/{config.name}/etc/profile.d/planeshift.sh"
        else:
            file = f"/home/{config.name}/.bashrc"
        fm = FileMarker(file)

        # Write the ssh keys into the authorized_keys file
        logger.debug(f"EnvApplier(apply): Processing environment for {config.name}...")
        exports = []
        for key in config.env.keys():
            exports.append(f"export {key}={config.env[key]}\n")

        # Write the exports into the bashrc file
        logger.debug(f"EnvApplier(apply): Adding exports to {file}...")
        fm.write_section(exports)

        # Ensure correct owner and permissions on the bashrc file
        if not config.chroot:
            logger.debug(f"EnvApplier(apply): Fixing permissions...")
            os.chown(file, config.uid, config.gid)
            os.chmod(file, stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | stat.S_IROTH)  # chmod 644


class ChrootApplier(Applier):
    """Applies the chroot configuration"""

    def apply(self, config: UserConfig) -> None:
        logger.debug(f"ChrootApplier(apply)")

        chroot_dir = f"/srv/chroot/planeshift/{config.name}"
        sshd_config_file = f"/etc/ssh/sshd_config.d/planeshift_{config.name}.conf"
        sudo_mount_points = {
            "/proc": os.path.join(chroot_dir, "proc"),
            "/sys": os.path.join(chroot_dir, "sys"),
            "/dev": os.path.join(chroot_dir, "dev"),
            "/dev/pts": os.path.join(chroot_dir, "dev/pts"),
        }

        # If the chroot is enabled for the user
        if config.chroot:
            logger.debug(f"ChrootApplier(apply): Chrooting user {config.name}...")
            # Process only if the chroot dir doesn't exist
            if not os.path.exists(chroot_dir):
                logger.debug(f"ChrootApplier(apply): Creating chroot folder {chroot_dir}...")
                # Create the chroot directory
                os.makedirs(chroot_dir)

            # Run debootstrap
            logger.debug(f"ChrootApplier(apply): Calling debootstap...")
            subprocess.run(["debootstrap", config.chroot_suite, chroot_dir])

            # Create target user within the chroot
            logger.debug(f"ChrootApplier(apply): Creating user and group inside the chroot...")
            subprocess.run(["chroot", chroot_dir, "groupadd", "-g", str(config.gid), config.name])
            subprocess.run(
                ["chroot", chroot_dir, "useradd", "-u", str(config.uid), "-g", str(config.gid), "-m", config.name]
            )

            # Copy resolv.conf and apt sources
            logger.debug(f"ChrootApplier(apply): Copying apt configuration and resolv.conf...")
            subprocess.run(["rsync", "-aL", "/etc/resolv.conf", os.path.join(chroot_dir, "etc")])
            subprocess.run(["rsync", "-aL", "/etc/apt", os.path.join(chroot_dir, "etc")])
            
            # Install packages
            logger.debug(f"ChrootApplier(apply): Updating APT...")
            subprocess.run(["chroot", chroot_dir, "apt", "update", "-y"])
            logger.debug(f"ChrootApplier(apply): Installing packages...")
            subprocess.run(["chroot", chroot_dir, "apt", "install", " ".join(config.chroot_packages), "-y"])

            # Create sshd configuration
            logger.debug(
                f"ChrootApplier(apply): Configuring SSH ({sshd_config_file}) to force the user into the chroot..."
            )
            with open(sshd_config_file, "w") as fd:
                fd.write(f"Match User {config.name}\n  ChrootDirectory {chroot_dir}\n")

            logger.debug(f"ChrootApplier(apply): Restarting the SSH service...")
            subprocess.run(["systemctl", "restart", "sshd.service"])

            # Bind mounts necessary for sudo to work if allowed
            if config.sudo_commands:
                logger.debug(f"ChrootApplier(apply): Creating bind mounts to support sudo...")
                for mount_point in sudo_mount_points.keys():
                    if not os.path.ismount(sudo_mount_points[mount_point]):
                        subprocess.run(["mount", "--bind", mount_point, sudo_mount_points[mount_point]])
        else:
            # If chroot is disabled and the chroot directory exists, delete the chroot
            if os.path.exists(chroot_dir):
                logger.debug(f"ChrootApplier(apply): Chroot detected for user {config.name}. Destroying...")
                # Unmount the bind mounts
                for mount_point in sudo_mount_points.values():
                    logger.debug(f"ChrootApplier(apply): Unmounting binds for sudo, if any...")
                    if os.path.ismount(mount_point):
                        subprocess.run(["umount", "-lf", mount_point])

                # Delete sshd configuration if it exists
                logger.debug(f"ChrootApplier(apply): Reconfiguring SSH to not chroot the user...")
                if os.path.exists(sshd_config_file):
                    os.remove(sshd_config_file)

                logger.debug(f"ChrootApplier(apply): Restarting the SSH service...")
                subprocess.run(["systemctl", "restart", "sshd.service"])

                # Delete the chroot directory
                logger.debug(f"ChrootApplier(apply): Deleting the chroot folder ({chroot_dir})...")
                shutil.rmtree(chroot_dir)
