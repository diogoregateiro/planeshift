from __future__ import annotations

import logging
from typing import Dict, List

import yaml

logger = logging.getLogger("Planeshift")


class SectionConfig:
    def __init__(self, config: dict = {}) -> None:
        logger.debug(f"SectionConfig(__init__)")
        self.name: str = config.get("name", None)
        self.default_shell: str = config.get("default_shell", "/bin/bash")
        self.sudo_commands: List[str] = config.get("sudo_commands", [])
        self.chroot: bool = config.get("chroot", False)
        self.chroot_suite: str = config.get("chroot_suite", "jammy")
        self.chroot_packages: List[str] = config.get("chroot_packages", [])
        self.ssh_keys: List[str] = config.get("ssh_keys", [])
        self.unix_groups: List[str] = config.get("unix_groups", [])
        self.env: Dict[str, str] = config.get("env", {})

    def set_name(self, name: str) -> None:
        self.name = name

    def set_default_shell(self, default_shell: str) -> None:
        self.default_shell = default_shell

    def add_sudo_commands(self, sudo_commands: List[str]) -> List[str]:
        self.sudo_commands = list(set(self.sudo_commands + sudo_commands))
        return self.sudo_commands

    def set_chroot(self, chroot: bool) -> None:
        self.chroot = chroot

    def set_chroot_suite(self, chroot_suite: str) -> None:
        self.chroot_suite = chroot_suite

    def add_chroot_packages(self, chroot_packages: List[str]) -> List[str]:
        self.chroot_packages = list(set(self.chroot_packages + chroot_packages))
        return self.chroot_packages

    def add_ssh_keys(self, ssh_keys: List[str]) -> List[str]:
        self.ssh_keys = list(set(self.ssh_keys + ssh_keys))
        return self.ssh_keys

    def add_unix_groups(self, unix_groups: List[str]) -> List[str]:
        self.unix_groups = list(set(self.unix_groups + unix_groups))
        return self.unix_groups

    def add_env(self, env: Dict[str, str]) -> Dict[str, str]:
        self.env = self.env | env
        return self.env


class UserConfig(SectionConfig):
    def __init__(
        self,
        name: str = None,
        uid: int = None,
        gid: int = None,
        user_groups: List[str] = [],
        infra_groups: List[str] = [],
        current_shell: str = None,
        default_shell: str = None,
        sudo_commands: List[str] = [],
        chroot: bool = False,
        chroot_suite: str = False,
        chroot_packages: List[str] = [],
        ssh_keys: List[str] = [],
        unix_groups: List[str] = [],
        env: Dict[str, str] = {},
    ) -> None:
        logger.debug(f"UserConfig(__init__)")
        self.name: str = name
        self.uid: int = uid
        self.gid: int = gid
        self.user_groups: List[str] = user_groups
        self.infra_groups: List[str] = infra_groups
        self.current_shell: str = current_shell
        self.default_shell = default_shell
        self.sudo_commands: List[str] = sudo_commands
        self.chroot: bool = chroot
        self.chroot_suite: str = chroot_suite
        self.chroot_packages: List[str] = chroot_packages
        self.ssh_keys: List[str] = ssh_keys
        self.unix_groups: List[str] = unix_groups
        self.env: Dict[str, str] = env

    def set_name(self, name: str) -> None:
        self.name = name

    def set_uid(self, uid: int) -> None:
        self.uid = uid

    def set_gid(self, gid: int) -> None:
        self.gid = gid

    def set_current_shell(self, current_shell: str) -> None:
        self.current_shell = current_shell

    def add_user_groups(self, user_groups: list) -> list:
        self.user_groups = list(set(self.user_groups + user_groups))
        return self.user_groups

    def add_infra_groups(self, infra_groups: list) -> list:
        self.infra_groups = list(set(self.infra_groups + infra_groups))
        return self.infra_groups

    def differs_from(self, target: UserConfig) -> bool:
        checks =[
            self.default_shell != target.default_shell,
            set(self.user_groups) != set(target.user_groups),
            set(self.infra_groups) != set(target.infra_groups),
            set(self.unix_groups) != set(target.unix_groups),
            set(self.sudo_commands) != set(target.sudo_commands),
            self.chroot != target.chroot,
            self.chroot_suite != target.chroot_suite,
            set(self.chroot_packages) != set(target.chroot_packages),
            set(self.ssh_keys) != set(target.ssh_keys),
            self.env != target.env
        ]
        return any(checks)

    def merge_configuration(self, config: SectionConfig) -> None:
        """Merges a section configuration into this user configuration

        Args:
            config (SectionConfig): The section configuration
        """
        self.set_default_shell(config.default_shell)
        self.set_chroot(config.chroot)
        self.set_chroot_suite(config.chroot_suite)
        self.add_chroot_packages(config.chroot_packages)
        self.add_sudo_commands(config.sudo_commands)
        self.add_ssh_keys(config.ssh_keys)
        self.add_unix_groups(config.unix_groups)
        self.add_env(config.env)


class InfraConfig:
    def __init__(self, config: dict = {}) -> None:
        logger.debug(f"InfraConfig(__init__)")
        infra_config = config.get("infra", {})
        self.enabled: bool = infra_config.get("enabled", False)
        self.version: str = infra_config.get("version", "0.21.0")
        self.server: str = infra_config.get("server", None)
        self.token: str = infra_config.get("token", None)

        # Sanity check if infra integration is enabled
        if self.enabled:
            # Check that an infra server url has been provided
            if not self.server:
                raise ValueError("Infra server URL is required if integration is enabled.")

            # Check that an infra authorization token has been provided
            if not self.token:
                raise ValueError("Infra authorization token is required if integration is enabled.")

    def set_enabled(self, enabled: bool) -> None:
        self.enabled = enabled

    def set_version(self, version: str) -> None:
        self.version = version

    def set_server(self, server: str) -> None:
        self.server = server

    def set_token(self, token: str) -> None:
        self.token = token


class AppConfig:
    def __init__(self, config_path: str) -> None:
        logger.debug(f"AppConfig(__init__)")

        # Read configuration
        with open(config_path, "r") as fd:
            config = yaml.safe_load(fd) or {}

        # Parse log level
        log_level = str(config.get("log_level", "INFO")).upper()
        log_warning = False
        if log_level not in ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]:
            log_warning = True
            log_level = "INFO"

        # Warn if log level was not valid
        if log_warning:
            logger.warning("Invalid log level detected, defaulted to INFO.")

        # Set log level
        logger.setLevel(log_level)

        self.sleep_time: int = config.get("sleep_time", 60)
        self.default: SectionConfig = SectionConfig(config.get("default", {}))
        self.groups: list[SectionConfig] = []
        self.users: list[SectionConfig] = []
        self.infra: InfraConfig = InfraConfig(config)

        # Initialize the groups configuration
        group_configs = config.get("groups", [])
        for group_config in group_configs:
            # Sanity Check
            if not group_config.get("name", None):
                raise ValueError("Group configuration found without the name, which is required.")

            # Append the group configuration
            self.groups.append(SectionConfig(group_config))

        # Initialize the users configuration
        user_configs = config.get("users", [])
        for user_config in user_configs:
            # Sanity Check
            if not user_config.get("name", None):
                raise ValueError("User configuration found without the name, which is required.")

            # Append the user configuration
            self.users.append(SectionConfig(user_config))

        logger.info("Configuration loaded successfully.")
