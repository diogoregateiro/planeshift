import grp
import logging
import os
import pwd
from threading import Event, Thread
from typing import Dict

from integration.infra import InfraAPI
from utils.appliers import (
    Applier,
    ChrootApplier,
    EnvApplier,
    ShellApplier,
    SSHKeysApplier,
    SudoersApplier,
    UnixGroupsApplier,
)
from utils.config import AppConfig, UserConfig

logger = logging.getLogger("Planeshift")


class Planeshift:
    """Manages new unix user accounts based on provided configuration"""

    def __init__(self, config: str) -> None:
        """Initializes the class

        Args:
            config (str): User configuration file path
        """
        # Init class variables
        logger.debug(f"Planeshift(__init__)")
        logger.info("Initializing Planeshift...")
        self._thread: Thread = Thread(target=self.__main_loop)
        self._terminate: Event = Event()
        self._config_path: str = config
        self._infra_api: InfraAPI = None
        self._processedUsers: Dict[str, UserConfig] = {}
        self._appliers: Dict[Applier] = [
            ChrootApplier(),
            ShellApplier(),
            SudoersApplier(),
            SSHKeysApplier(),
            UnixGroupsApplier(),
            EnvApplier(),
        ]

    def start(self) -> None:
        """Starts the planeshift thread"""
        logger.debug(f"Planeshift(start)")
        logger.info("Starting Planeshift...")
        self._terminate.clear()
        self._thread.start()

    def join(self) -> None:
        """Joins the planeshift thread"""
        logger.debug(f"Planeshift(join)")
        self._thread.join()

    def terminate(self) -> None:
        """Terminates the planeshift thread"""
        logger.debug(f"Planeshift(terminate)")
        logger.info("Terminating Planeshift...")
        self._terminate.set()

    def __configurable_user_account(self, account) -> bool:
        """Checks if the account is configurable by Planeshift

        Args:
            account (str): Unix account from pwd

        Returns:
            bool: Whether the account is configurable or not
        """
        logger.debug(f"Planeshift(__configurable_user_account)")
        try:
            return account.pw_dir.startswith("/home") and os.path.exists(account.pw_dir)
        except KeyError:
            return False

    def __match_infra_user_account(self, account: str) -> str:
        """Matches the unix account name to an infra user

        Args:
            account (dict): Unix account name

        Returns:
            str: The infra user ID that matches the given account name
        """
        logger.debug(f"Planeshift(__match_infra_user_account)")
        infra_user = [u for u in self._infra_api.get_all_users() if u["sshLoginName"] == account]
        if infra_user:
            return infra_user[0]["id"]

    def __get_user_configuration(self, app_config: AppConfig, user_config: UserConfig) -> None:
        """Gets the final user configuration

        Args:
            user_config (UserConfig): The user data
        """
        logger.debug(f"Planeshift(__get_user_configuration)")

        # Set default configuration
        user_config.merge_configuration(app_config.default)

        # Set groups configuration
        for ps_group_config in app_config.groups:
            if ps_group_config.name in (user_config.user_groups + user_config.unix_groups + user_config.infra_groups):
                user_config.merge_configuration(ps_group_config)

        # Set user configuration
        for ps_user_config in app_config.users:
            if user_config.name == ps_user_config.name:
                user_config.merge_configuration(ps_user_config)

    def __main_loop(self) -> None:
        """Main planeshift loop"""
        logger.debug(f"Planeshift(__main_loop)")

        # While not terminated
        while not self._terminate.is_set():
            logger.info("Checking for new users to configure...")

            # Load the Planeshift config
            app_config = AppConfig(self._config_path)

            # Initialize InfraAPI if enabled
            if app_config.infra.enabled:
                if not self._infra_api:
                    logger.info("Initializing infra integration...")
                    self._infra_api = InfraAPI(app_config.infra)
            else:
                self._infra_api = None

            # For each entry in the password database that is a user account
            for account in [acc for acc in pwd.getpwall() if self.__configurable_user_account(acc)]:
                # Get the required user data
                user_config = UserConfig()
                user_config.set_name(account.pw_name)
                user_config.set_uid(account.pw_uid)
                user_config.set_gid(account.pw_gid)
                user_config.set_current_shell(account.pw_shell)

                # Set the current unix groups for the user
                user_config.add_user_groups([g.gr_name for g in grp.getgrall() if user_config.name in g.gr_mem])

                # If the infra integration is enabled, fetch the groups
                if app_config.infra.enabled:
                    infra_uid = self.__match_infra_user_account(user_config.name)
                    if infra_uid:
                        user_config.add_infra_groups([g["name"] for g in self._infra_api.get_user_groups(infra_uid)])

                # Collapse the planeshift configuration for this user
                self.__get_user_configuration(app_config, user_config)

                # If the user has not been processed or the configuration changed
                if user_config.name not in self._processedUsers.keys() or self._processedUsers[
                    user_config.name
                ].differs_from(user_config):
                    logger.info(f"Applying configuration for user {user_config.name}...")

                    # Apply the configuration
                    for applier in self._appliers:
                        applier.apply(user_config)

                    # Set the user as processed
                    self._processedUsers[user_config.name] = user_config

            # Sleep for a minute
            logger.info(f"Finished, going to nap for {app_config.sleep_time} seconds.")
            self._terminate.wait(app_config.sleep_time)
