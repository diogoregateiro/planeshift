VERSION=1.0.0-1

.PHONY: format lint

package:
	cd packaging ; make package
	mv -f planeshift_$(VERSION)_amd64.* releases/

install: package
	sudo apt-get install ./releases/planeshift_$(VERSION)_amd64.deb -y

remove:
	sudo apt-get autoremove planeshift -y
	sudo apt-get purge planeshift -y

reinstall: remove install

format:
	poetry run isort --profile black --line-length 120 src/
	poetry run black --line-length 120 src/

lint:
	poetry run isort --check-only --profile black --line-length 120 src/
	poetry run black --check --line-length 120 src/
