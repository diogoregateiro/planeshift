import logging
from urllib.parse import urljoin

import requests_cache

from utils.config import InfraConfig

logger = logging.getLogger("Planeshift")


class InfraAPI:
    def __init__(self, config: InfraConfig) -> None:
        logger.debug(f"InfraAPI(__init__)")
        self._infra_api_url = urljoin(config.server, "api/")
        self._infra_headers = {
            "Content-Type": "application/json",
            "Infra-Version": f"{config.version}",
            "Authorization": f"Bearer {config.token}",
        }
        self._session = requests_cache.CachedSession("infra", expire_after=60)

    def get_all_users(self):
        """Requests the list of users registered with infra.

        Raises:
            Exception: When the infra server does not return status code 200.

        Returns:
            dict: An array with the infra users.
        """
        logger.debug(f"InfraAPI(get_all_users)")
        return self.__infra_api_get("users")

    def get_user_groups(self, userID: str) -> dict:
        """Requests the list of groups a given infra user belongs to.

        Args:
            userID (str): The ID of the infra user.

        Raises:
            Exception: When the infra server does not return status code 200.

        Returns:
            dict: An array with the user's groups.
        """
        logger.debug(f"InfraAPI(get_user_groups):")
        return self.__infra_api_get("groups", {"userID": userID})

    def __infra_api_get(self, endpoint: str, params: dict = {}) -> dict:
        """Makes a GET request to the infra server.

        Args:
            endpoint (str): The infra API endpoint to send the request to.
            params (dict): The query parameters to send with the request.

        Raises:
            Exception: When the infra server does not return status code 200.

        Returns:
            dict: An array with the data items.
        """
        logger.debug(f"InfraAPI(__infra_api_get)")

        # Set the first page of the request to 1
        params["page"] = 1

        # Handle request pagination
        data = []
        while True:
            # Request a page of data
            response = self._session.get(
                url=urljoin(self._infra_api_url, endpoint),
                headers=self._infra_headers,
                params=params,
            )

            # Ensure API status code is 200
            if response.status_code != 200:
                logger.error(f"Could not obtain data from Infra server. HTTP {response.status_code}.")
                return None

            # Convert the content to json
            content = response.json()

            # Append the received data
            data.extend(content["items"])

            # Break the loop after handling the last page
            if params["page"] >= content["totalPages"]:
                break

            # Update the params to request the next page
            params["page"] = params["page"] + 1

        # Return the data
        return data
