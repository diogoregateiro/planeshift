import logging
import os
from typing import List

logger = logging.getLogger("Planeshift")


class FileMarker:
    """Manages Planeshift sections in existing configuration files"""

    def __init__(self, file: str, comment_symbol: str = "#") -> None:
        """Initializes a FileMarker object

        Args:
            file (str): Path to a configuration file
            comment_symbol (str, optional): The comment symbol for the target file. Defaults to "#".
        """
        logger.debug(f"FileMarker(__init__)")
        self._start_marker: str = f"{comment_symbol}__planeshift_section_start"
        self._end_marker: str = f"{comment_symbol}__planeshift_section_end"
        self._file: str = file

    def read_section(self) -> List[str]:
        """Reads a text section into the file using a marker.

        Returns:
            List[str]: The contents of the section inside the file
        """
        logger.debug(f"FileMarker(read_section)")
        content = []
        if os.path.exists(self._file) and os.path.isfile(self._file):
            with open(self._file, "r") as fd:
                in_section = False
                for line in fd.readlines():
                    # If the end marker is found, unset in_section
                    if line.startswith(self._end_marker):
                        in_section = False

                    # If in_section, save the contents
                    if in_section:
                        content.append(line)

                    # If the start marker is found, set in_section
                    if line.startswith(self._start_marker):
                        in_section = True

        return content

    def write_section(self, lines: List[str]) -> None:
        """Inserts a text section into the file using a marker. If the section already exists, it will be replaced.

        Args:
            lines (List[str]): The text to insert to the configuration file.
        """
        logger.debug(f"FileMarker(write_section)")
        clean_file = []
        if os.path.exists(self._file) and os.path.isfile(self._file):
            with open(self._file, "r") as fd:
                in_section = False
                for line in fd.readlines():
                    # If the start marker is found, set in_section
                    if line.startswith(self._start_marker):
                        in_section = True

                    # If not in_section, save the contents
                    if not in_section:
                        clean_file.append(line)

                    # If the end marker is found, unset in_section
                    if line.startswith(self._end_marker):
                        in_section = False

        with open(self._file, "w") as fd:
            # Write the clean file without the planeshift section
            fd.writelines(clean_file)

            # Insert the start marker
            fd.write(self._start_marker + "\n")

            # Insert the text
            for line in lines:
                # Ensure the line ends with a newline
                if not line.endswith("\n"):
                    line = line + "\n"
                fd.write(line)

            # Insert the end marker
            fd.write(self._end_marker + "\n")
