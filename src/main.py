import argparse
import logging
import logging.handlers
import os
import signal

import yaml

from planeshift import Planeshift


# Main function that reads the config
def main():
    """Service entrypoint"""
    # Configure the script arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("config")
    args = parser.parse_args()

    # Configure Logger
    logging.basicConfig(encoding="utf-8")
    logger = logging.getLogger("Planeshift")
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    handler = logging.handlers.SysLogHandler(address="/dev/log")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel("INFO")
    logger.propagate = False

    # Ensure it was run by root
    if not os.geteuid() == 0:
        logger.critical("Only root can run this script")
        exit(1)

    # Configure the Planeshift object
    try:
        planeshift = Planeshift(args.config)
    except ValueError:
        logger.critical("Invalid configuration.")
        exit(2)

    # Start the main processing thread
    planeshift.start()

    # Handle SIGTERM
    def handle_exit(sig, frame):
        """Handles sigterm exit"""
        planeshift.terminate()
        planeshift.join()

    signal.signal(signal.SIGTERM, handle_exit)


# Call the main function when the script is executed
if __name__ == "__main__":
    main()
