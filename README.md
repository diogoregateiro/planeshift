# Planeshift

Planeshift is a linux account configuration service that allows administrators to define some common configuration to be applied to user accounts.

User accounts can only be configurable if the account has a home directory inside `/home`, any other accounts on the system are skipped.

## Configuration Levels

User account configuration can be defined on three levels: `default`, `group`, and `user`.

* **Default**: default level configuration is applied to all (configurable) user accounts.
* **Group**: group level configuration is only applied to user accounts that are members of the specified group. The user account can be added as a member manually or through planeshift. If InfraHQ integration is enabled, planeshift will also match the InfraHQ groups to the user account (through the ssh login name).
* **User**: user level configuration applies only to the user with the specified name.

When there is configuration set on multiple levels, it is additive, meaning that nothing is overriden unless the configuration clashes (e.g. the same environment variable is defined). In such cases, the more specific configuration level takes precedence.

## Configuration Options

The current release of planeshift allows to configure the following:

* Set the default shell. This is done using `chsh -s <shell> <user>`.
* Set the list of authorized SSH keys used to login to the account. This is done by adding the specified keys to the `.ssh/authorized_keys` file in the user account's home directory.
* Set the linux groups to add to the account (removing a group from this list after it has been applied doesn't remove the account from the group). This is done using `usermod -a -G <group> <user>`
* Set environment variables to be exported for login shells. This is done by adding a list of export commands to the `.bashrc` file in the user account's home directory (or to the `/etc/profile.d/planeshift.sh` file when inside a chroot).
* Set a list of allowable sudo commands that the user can execute. This is done by creating the `/etc/sudoers.d/planeshift` file with the allowed commands for each user.
* Chroot the user. This is done by using `debootstrap` to create a minimal OS instalation for a user and modifying the sshd configuration to force the chroot on login.

The whole process can be verified in the [appliers source code](./src/utils/appliers.py).

## Installation

Download the deb file from the latest release and install it with APT:

```bash
wget https://gitlab.com/diogoregateiro/planeshift/-/raw/main/releases/planeshift_1.0.0-1_amd64.deb
sudo apt-get install ./planeshift_1.0.0-1_amd64.deb -y
```

The configuration file is located at `/etc/planeshift/config.yml`, and the logs can be viewed from the syslog (`/var/log/syslog`).

To verify that planeshift is running, systemctl can be used:

```bash
sudo systemctl status planeshift.service
```

Do note that the service automatically loads changes made to the configuration file, there is no need to restart the service.
